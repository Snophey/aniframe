--[[

This table forms the namespace of our animation framework.

aniframe = {
	-- most important function
	aniframe.animation(steps) : table,

	-- more on these later
	function aniframe.update(self) : table,
	function aniframe.pause(self) : nil,
	function aniframe.reset(self) : nil,
}

function: aniframe.animation(steps) : table


Section I: On steps

steps is a table containing states of an animation, as well as an amount of frames given to achieve that state. Each step/state is a table containing parameters that change during the animation. There is no limit to the amount of steps. 

example:
steps = {
	{
		scale = 1, r = 255, g = 0, b = 255,
	},
	60, {
		scale = 3, g = 255
	}
}

This means that an object has an initial scale factor of 1 which gradually changes to 3 over the course of 60 frames. During that time it also turns white as its [g]reen value becomes 255.

In a steps table odd indices signify a step while under the (preceding) even index the amount of frames required to reach that state/step can be found.

IMPORTANT: numbers under even indices (from now on referred to as 'frames') always have to be 1 or larger. Negative numbers or 0 will mess up/crash your animation/application.
NOTE: By adding 'loop = true' or 'paused = true' you can override animation object's defaults (more on these variables in section III) 


Section II: what animation(steps) does

Brief: animation receives the list of steps and generates an Animation object. (More on this object in section III)


Detailed:
This is the algorithm of animation(steps):
	I.	 Generation of the function "initial()"
			1. declare function 'initial'
			2. copy all entries of steps[1] to a local table 'initstate'
			3. make 'initial' return 'initstate'
	II.  Generation of mods
			1. iterate over steps using modgen iterator with f for frame number and m for mod table
			2. on each iteration do 'mods[#mods+1] = {f, m}' with 'mods' being a local table
	III. Creation of the animation object
			1. local anim = {frame = 1, paused = false, loop = false, mods = mods, initial = initial, current_state = initial()}
			2. set anim's metatable to {__index = aniframe}
	IV.	 Return anim


Section III: the Animation object

Members:
	- function intial() : table
	- function aniframe.update(self) : table
	- function aniframe.pause(self) : nil
	- function aniframe.reset(self) : nil

	- frame : number
	- mod_index : number
	- paused : boolean
	- loop : boolean
	- mods : table
	- current_state : table

function initial() : table
	a closure which returns a table describing the initial state of the animation. 

function update(self) : table
	if paused == false, the function changes current_state by applying modifications from the current mods-instance, eventually switches mods - entries and if all of them have been processed, self:pause() is called (unless loop is true, in this case self:reset() will be called instead).
	The returned table is always current_state.

function pause(self, s) : nil
	sets paused to 's'(true/false), thus preventing update() from changing the current state/resuming the animation

function reset(self) : nil
	sets current frame as well as mod_index to 1 and does 'self.current_state=self.initial()' to reset the state.

frame : number
	number of the current frame (used to index mods entries)

paused : boolean
	true if animation is well... paused otherwise false

loop : boolean
	if this is true then reset(self) will be called each time all mods entries have been processed in order to restart the animation

mods : table
	This table contains modifications per frame for each parameter. An individual mods entry is generated for each step passed to
	animation(steps) (except for the initial state). Each entry is a table and is referenced by the frame number where it starts.
	There is a special variable found under the "n" key, which is the amount of tables in mods.

current_state : table
	all changes from mods entries are applied to this table

mod_index : number
	used to access mods-table entries


Section IV: modgen iterator

The modgen function is an iterator factory. It expects the steps-table as its only argument. The function it returns, generates a table which later tells the update function how much current_state members have to be increased per one frame in order to finish the animation after the given amount of frames.
E.g. if we wanted to make a fade a fade in effect by gradually changing alpha from 0 to 255 in 100 frames then our iterator's output would be:

{
	-- because (255-0)/100=2.55
	alpha = 2.55
}

]]

do

aniframe = {}

-- warning: this does not work for nested tables
local function copytable(t)
	local a = {}
	for i,v in pairs(t) do
		a[i] = v
	end

	return a
end

local function modgen(steps)
	local i = 0

	for s=3,#steps,2 do
		setmetatable(steps[s], {__index=steps[s-2]})
	end

	local fsum = 0

	return function()
		i = i+2
		local mod = {}
		local frames = steps[i]

		if frames == nil then
			return nil, nil
		end

		fsum = frames + fsum

		for k,v in pairs(steps[i+1]) do
			-- more readable equivalent:
			-- mod[key] = (new_step[key] - current_step[key])/frames
			local t = type(v)
			if t == "number" then
				mod[k] = (v-steps[i-1][k])/frames
			elseif t == "string" then
				mod[k] = v
			end
		end

		return fsum, mod
	end
end

function aniframe.animation(steps)
	-- I.
	local initial = function()
		local initstate = copytable(steps[1])
		return initstate
	end

	-- II.
	local mods = {n=(#steps-1)/2}
	for f,m in modgen(steps) do
		mods[#mods+1] = {f, m}
	end

	-- III.
	local anim = {
		frame = 1, paused = steps.paused or false, loop = steps.loop or false, ["mods"] = mods,
		["initial"] = initial, current_state = initial(), mod_index = 1,
	}
	setmetatable(anim, {__index=aniframe})

	-- IV.
	return anim
end

local function modify_state(state, mod)
	for k,v in pairs(mod) do
		local t = type(v)
		if t == 'number' then
			state[k] = v+(state[k] or 0)
		elseif t == 'string' then
			state[k] = v
		end
	end
end

function aniframe.pause(self, s)
	self.paused = s
end

function aniframe.reset(self)
	self.mod_index = 1
	self.frame = 1
	self.current_state = self.initial()
end

function aniframe.update(self)
	if (self.paused == false) and (self.mod_index <= self.mods.n) then
		modify_state(self.current_state, self.mods[self.mod_index][2])
		self.frame = self.frame + 1

		if self.frame > self.mods[self.mod_index][1] then
			self.mod_index = self.mod_index + 1

			if self.mod_index > self.mods.n then
				self:pause(true)

				if self.loop then self:reset(); self:pause(false) end
			end
		end
	end

	return self.current_state
end

end
