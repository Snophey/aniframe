
dofile "animation.lua"

test = aniframe.animation{
	{r = 255, g = 0, b = 255, rot=6.28318531},
	60, {r = 255, g = 255, b = 255, rot=0},
	60, {g = 0, rot=-6.28318531}, paused=true, loop=true
}
test.loop = true

function love.draw()
	r = test:update()

	love.graphics.setColor(r.r, r.g, r.b)
	love.graphics.print("Press any key to (un)pause", 400, 400, r.rot, 2, 2)
end

function love.keypressed()
	test:pause(not test.paused)
end
